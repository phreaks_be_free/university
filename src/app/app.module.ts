import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ResumeModule } from './resume/resume.module';
import { NavigationComponent } from './resume/navigation/navigation.component';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { NgxGistModule } from 'ngx-gist/dist/ngx-gist.module';
import { HighlightService } from './resume/services/highlighter-service';
import { PercentBarComponent } from './percent-bar/percent-bar.component';


@NgModule({
    declarations: [
        AppComponent,
        PercentBarComponent,
    ],
    imports: [
        CommonModule,
        BrowserModule,
        ResumeModule,
        NgxGistModule,
        RouterModule.forRoot([
            {
                path: '',
                component: NavigationComponent

            }
        ]),
    ],
    providers: [
        HighlightService
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
