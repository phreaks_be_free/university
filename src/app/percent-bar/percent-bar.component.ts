import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'university-percent-bar',
  templateUrl: './percent-bar.component.html',
  styleUrls: ['./percent-bar.component.scss']
})
export class PercentBarComponent implements OnInit {

  constructor() { }

  @Input() name: string;
  @Input() percent: number;

  ngOnInit() {
  }

}
