export class Work {
    id = 1;
    title = 'Example project article';
    author = 'Morgan Kuta';
    tag = 'Dummy Project';

    thumb_url = 'https://images.unsplash.com/photo-1498050108023-c5249f4df085?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1352&q=80';

    content = [
        {
            /* first paragraph */
            p: `Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Cras ultricies ligula sed magna dictum
                    porta. Quisque velit
                    nisi, pretium ut lacinia in, elementum id enim. Praesent sapien massa, convallis a pellentesque
                    nec, egestas non nisi. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere
                    cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula.
                    Nulla quis lorem ut libero malesuada feugiat.`,
            /* blockquote  */
            bq: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.`,
            /* code to be embeded */
            code: {
                title: 'Example CSS',
                language: 'css',
                content: `p {
                  color: red;
                  }`
            }

        },
        {
            /* second paragraph */
            p: `Nulla porttitor accumsan tincidunt. Cras ultricies ligula sed magna dictum porta. Mauris blandit
                    aliquet elit, eget tincidunt
                    nibh pulvinar a. Cras ultricies ligula sed magna dictum porta. Lorem ipsum dolor sit amet,
                    consectetur adipiscing elit. Donec sollicitudin molestie malesuada.`,
            code: {
                title: 'Example PHP',
                language: 'php',
                content: `$some_var = 5;
$otherVar = "Some text";
$null = null;
$false = false;`
            }
        },
        {
            /* next paragraph ... */
            p: `Nulla porttitor accumsan tincidunt. Cras ultricies ligula sed magna dictum porta. Mauris blandit
                    aliquet elit, eget tincidunt
                    nibh pulvinar a. Cras ultricies ligula sed magna dictum porta. Lorem ipsum dolor sit amet,
                    consectetur adipiscing elit. Donec sollicitudin molestie malesuada.`,
            code: {
                title: 'Example JS',
                language: 'javascript',
                content: `let foo = \'bar\';
baz();
console.log(\'It Works!\');`
            }
        },
        {
            /* next paragraph ... */
            p: `Nulla porttitor accumsan tincidunt. Cras ultricies ligula sed magna dictum porta. Mauris blandit
                    aliquet elit, eget tincidunt
                    nibh pulvinar a. Cras ultricies ligula sed magna dictum porta. Lorem ipsum dolor sit amet,
                    consectetur adipiscing elit. Donec sollicitudin molestie malesuada.`,
            code: {
                title: 'Example HTML',
                language: 'html',
                content: `<div class="example">
<p>This example WORKS!</p>
</div>`
            }
        }

    ]
}