abstract class Serializable {
    public constructor(init?) {
        Object.assign(this, init);
    }
}

export class Lineup extends Serializable {
    id: number;
    value: string;
    resume_attribute: any;
    page: any;
    type: any;
    created_at: any;
    updated_at: any;
    public constructor(init: Partial<Lineup>) {
        super(init);
    }

}

export class Resume extends Serializable {
    id: number;
    name: string;
    user_id: number;
    created_at: any;
    updated_at: any;
    lineup: Lineup[];
    public constructor(init?: Partial<Resume>) {
        super(init);
        this.lineup = init.lineup.map((l) => {
            return new Lineup(l);
        });
    }

}



// export class Resumes extends Serializable {
//     resumes: Resume[] = [];

//     public constructor(init?: Partial<Resumes>) {
//         super(init);
//         this.resumes = init.resumes.map((r) => {
//             return new Resume(r);
//         });
//     }
// }

