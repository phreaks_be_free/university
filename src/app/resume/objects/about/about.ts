import { Lineup } from '../resume/resume';


export class About {
    dev_name = '';
    profile = '';
    email = '';
    phone = '';
    dev_picture_url = '';
    description = '';
    skills = {};
    education = '';
    private lineup: Lineup[];
    public constructor(init?) {
        this.lineup = init.page.map((l) => {
            return new Lineup(l);
        });

        for (const l of this.lineup) {
            if (this.hasOwnProperty(l.type.name)) {
                this[l.type.name] = l.value;
            }
        }
    }

}
