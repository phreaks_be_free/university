import { Component, OnInit, ViewEncapsulation, ViewChild, ElementRef, HostListener, Inject } from '@angular/core';
import { ResumeService } from '../services/resume-service';
import { Resume } from '../objects/resume/resume';
import { WINDOW } from 'src/app/resume/services/window-service';
import { DOCUMENT } from '@angular/common';

@Component({
    selector: 'university-navigation',
    templateUrl: './navigation.component.html',
    styleUrls: ['./navigation.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class NavigationComponent implements OnInit {

    public resume: Resume;
    public isScrolled: boolean = false;
    public navbarExpanded: boolean = false;

    @ViewChild('navbar') navbarEl: ElementRef;
    @ViewChild('preloader') preloaderEl: ElementRef;
    @ViewChild('backToTop') backToTopEl: ElementRef;

    @ViewChild('home') homeEl: ElementRef;
    @ViewChild('about') aboutEl: ElementRef;
    // @ViewChild('services') servicesEl: ElementRef;
    @ViewChild('work') workEl: ElementRef;

    public currentActive: number;
    public homeOffset: number;
    public aboutOffset: number;
    // public servicesOffset: number;
    public workOffset: number;

    constructor(
        private resumeService: ResumeService,
        @Inject(DOCUMENT) private document: Document,
        @Inject(WINDOW) private window: Window
    ) {
        // this.resumeService.get_resume_by_url('http://localhost:4200')
        //     .subscribe(r => console.log(r));
        // this.resumeService.get_resume_page_by_url('http://localhost:4200', 'about')
        //     .subscribe(r => console.log(r));
    }

    @HostListener('window:scroll', [])
    onWindowScroll() {
        let currentScroll: number = this.window.pageYOffset || this.document.documentElement.scrollTop || this.document.body.scrollTop || 0;
        if (currentScroll > 100) {
            this.isScrolled = true;
        } else if (this.isScrolled && currentScroll < 10) {
            this.isScrolled = false;
        }

        if (currentScroll >= this.homeOffset && currentScroll < this.aboutOffset) {
            this.currentActive = 1;
        } else if (currentScroll >= this.aboutOffset && currentScroll < this.workOffset) {
            this.currentActive = 2;
        // } else if (currentScroll >= this.servicesOffset && currentScroll < this.workOffset) {
        //     this.currentActive = 3;
        } else if (currentScroll >= this.workOffset) {
            this.currentActive = 3;
        } else {
            this.currentActive = 0;
        }
    }

    reduceNavbar() {
        this.navbarExpanded = !this.navbarExpanded;
        let elClasses = this.navbarEl.nativeElement.classList;
        if (!elClasses.contains('navbar-reduce')) {
            elClasses.add('navbar-reduce');
        }
    }


    scrollToTop() {
        let currentScroll: number = this.window.pageYOffset || this.document.documentElement.scrollTop || this.document.body.scrollTop || 0;
        if (currentScroll > 100) {
            window.scrollTo({ left: 0, top: 0, behavior: 'smooth' });
        }
    }

    scrollTo(el: number) {
        this.navbarExpanded = false;

        if (el === 1) {
            window.scrollTo({ left: 0, top: this.homeOffset + 1, behavior: 'smooth' });
        }
        if (el === 2) {
            window.scrollTo({ left: 0, top: this.aboutOffset + 1, behavior: 'smooth' });
        }
        // if (el === 3) {
        //     window.scrollTo({ left: 0, top: this.servicesOffset + 1, behavior: 'smooth' });
        // }
        if (el === 3) {
            window.scrollTo({ left: 0, top: this.workOffset + 1, behavior: 'smooth' });
        }
        // el.scrollIntoView({ behavior: 'smooth', block: 'center' });
    }

    ngOnInit() {
        setTimeout(() => {
            if (!this.preloaderEl.nativeElement.classList.contains('step')) {
                this.preloaderEl.nativeElement.classList.add('step');
                setTimeout(() => {
                    this.preloaderEl.nativeElement.remove();
                }, 500);
            }
        }, 500);
        this.homeOffset = this.homeEl.nativeElement.offsetTop;
        this.aboutOffset = this.aboutEl.nativeElement.offsetTop;
        // this.servicesOffset = this.servicesEl.nativeElement.offsetTop;
        this.workOffset = this.workEl.nativeElement.offsetTop;
    }
}
