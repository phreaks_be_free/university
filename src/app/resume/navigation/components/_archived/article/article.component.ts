import { Component, OnInit, AfterViewChecked, Inject } from '@angular/core';
import { HighlightService } from 'src/app/resume/services/highlighter-service';
import { Work } from 'src/app/resume/objects/work/work';

@Component({
  selector: 'university-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss']
})
export class ArticleComponent implements OnInit, AfterViewChecked {

  highlighted: boolean = false;
  
  // work will be fetched via api from main server
  public work: Work;


  constructor(@Inject (HighlightService) private highlightService: HighlightService) { 
    this.work = new Work();
  }

  ngAfterViewChecked() {
    if (!this.highlighted) {
      this.highlightService.highlightAll();
      this.highlighted = true;
    }
  }
  ngOnInit() {
  }

}
