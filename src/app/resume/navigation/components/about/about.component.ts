import { Component, OnInit, OnDestroy } from '@angular/core';
import { ResumeService } from '../../../services/resume-service';
import { About } from 'src/app/resume/objects/about/about';
import { Subscription } from 'rxjs';

@Component({
    selector: 'university-about',
    templateUrl: './about.component.html',
    styleUrls: ['./about.component.scss']
})

export class AboutComponent implements OnInit, OnDestroy {

    public about: About;
    private subscription: Subscription;
    constructor(resumeService: ResumeService) {
        this.subscription = resumeService.get_resume_page_by_url(window.location.href, 'about').subscribe((response: About) => {
            this.about = new About(response);
        });
    }

    ngOnInit() {

    }

    ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }

}
