import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import * as Typed from 'src/assets/typed/typed.js';
import { ResumeService } from 'src/app/resume/services/resume-service';
import { Intro } from 'src/app/resume/objects/intro/intro';
import { Subscription } from 'rxjs';
@Component({
  selector: 'university-intro',
  templateUrl: './intro.component.html',
  styleUrls: ['./intro.component.scss']
})
export class IntroComponent implements OnInit {

  
  @ViewChild('textSlider') textSlider: ElementRef;
  // @ViewChild('textSliderItems') textSliderItems: ElementRef;
  intro: Intro;
  private subscription: Subscription;

  constructor(private resumeService: ResumeService) { 
    this.subscription = this.resumeService.get_resume_page_by_url(window.location.href, 'intro').subscribe(response => {
      this.intro = new Intro(response);
      if (this.textSlider) {
        const typedStrings = this.intro.description;
  
        const typed = new Typed(this.textSlider.nativeElement, {
          strings: typedStrings.split(','),
          typeSpeed: 80,
          loop: true,
          backDelay: 1100,
          backSpeed: 30
        });
      }
    });
  }
  ngOnInit() {

    // if (this.textSlider) {
    //   const typedStrings = this.intro.description;

    //   const typed = new Typed(this.textSlider.nativeElement, {
    //     strings: typedStrings.split(','),
    //     typeSpeed: 80,
    //     loop: true,
    //     backDelay: 1100,
    //     backSpeed: 30
    //   });
    // }
  }

}
