import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError, map } from 'rxjs/operators';
import { Resume, Lineup } from 'src/app/resume/objects/resume/resume';
import { About } from '../objects/about/about';
interface PageResponse {
    page: any[];
}
@Injectable({
    providedIn: 'root'
})


export class ResumeService {
    constructor(private http: HttpClient) {
    }

    private httpOptions: any = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
            'Accept': 'json'
        })
    };

    private api_url = 'https://www.unicornoverlord.com/api/v1/resume/by_url/';

    get_resume_by_url(url: string): Observable<any> {
        return new Observable(o => {
            const data = JSON.stringify({ url });
            this.http.post(this.api_url + 'default', data, this.httpOptions)
                .pipe(retry(3))
                // .pipe(map(response => new Resume(response.resume)))
                .subscribe(response => {
                    o.next(response);
                });
        });
    }

    get_resume_page_by_url(url: string, pageName: string): Observable<any> {
        return new Observable(o => {
            const data = JSON.stringify({ url, page: pageName });
            this.http.post(this.api_url + 'default/page', data, this.httpOptions)
                .pipe(retry(3))
                // .pipe(map(response => {
                //     return new About(response);
                // }))
                .subscribe(response => {
                    o.next(response);
                });
        });
    }


}