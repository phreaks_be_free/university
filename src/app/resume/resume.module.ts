import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavigationComponent } from './navigation/navigation.component';
import { RateFiveModule } from '../rate-five/rate-five.module';
import { HttpClientModule } from '@angular/common/http';
import { AboutComponent } from './navigation/components/about/about.component';
import { ContactComponent } from './navigation/components/contact/contact.component';
import { IntroComponent } from './navigation/components/intro/intro.component';
import { WorkComponent } from './navigation/components/work/work.component';
import { ArticleComponent } from './navigation/components/article/article.component';
import { CounterComponent } from './navigation/components/counter/counter.component';
import { FooterComponent } from './navigation/components/footer/footer.component';
import { WINDOW_PROVIDERS } from './services/window-service';

@NgModule({
    declarations: [NavigationComponent,
        AboutComponent,
        WorkComponent,
        ContactComponent,
        IntroComponent,
        ArticleComponent,
        CounterComponent,
        FooterComponent,
    ],
    imports: [
        CommonModule,
        RateFiveModule,
        HttpClientModule
    ],
    providers: [WINDOW_PROVIDERS]
})
export class ResumeModule {
}
