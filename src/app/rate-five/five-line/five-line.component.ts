import {Component, Input, OnInit} from '@angular/core';

@Component({
    selector: 'university-five-line',
    templateUrl: './five-line.component.html',
    styleUrls: ['./five-line.component.scss']
})
export class FiveLineComponent implements OnInit {

    constructor() {
    }

    @Input() rating: number;
    @Input() title: string;

    fullIcon = 'fas fa-circle';
    emptyIcon = 'far fa-circle';
    lengthOfRating = 5;

    ngOnInit() {
    }

    get_icon_by_index_count(count: number) {
        if (count <= this.rating) {
            return this.fullIcon;
        }
        return this.emptyIcon;
    }

    range(count: number) {
        return new Array(count);
    }
}
