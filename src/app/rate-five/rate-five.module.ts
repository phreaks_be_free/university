import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FiveLineComponent} from './five-line/five-line.component';

@NgModule({
    declarations: [FiveLineComponent],
    imports: [
        CommonModule
    ],
    exports: [
        FiveLineComponent
    ]
})
export class RateFiveModule {
}
